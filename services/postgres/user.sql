CREATE TABLE user_account (
  id SERIAL PRIMARY KEY,
  first_name TEXT,
  last_name TEXT,
  email TEXT,
  zip_code TEXT
);
