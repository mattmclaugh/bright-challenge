package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
)

var requestDuration *prometheus.HistogramVec

func InitializeMetrics() {
	requestDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "request_duration_seconds",
			Help:    "A histogram of latencies for requests.",
			Buckets: []float64{0.0001, .25, 1.5, 5, 10, 36},
		},
		[]string{"handler", "method", "code"},
	)
	prometheus.MustRegister(requestDuration)
}

func MetricsHandler(handler string, next http.Handler) http.Handler {
	return promhttp.InstrumentHandlerDuration(requestDuration.MustCurryWith(prometheus.Labels{"handler": handler}), next)
}
