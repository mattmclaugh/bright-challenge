package main

import (
	"database/sql"
	"github.com/apex/log"
	apexJson "github.com/apex/log/handlers/json"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
	"os"
	"time"
)

type Service struct {
	Name        string
	Environment string
	DB          *sql.DB
	Logger      log.Interface
	Router      Router
}

func (s *Service) initializeRoutes() {
	http.Handle("/users", MetricsHandler("users", s.UsersHandler()))
	http.Handle("/user", MetricsHandler("user", s.UserHandler()))
	http.Handle("/user/", MetricsHandler("user/:id", s.UserHandler()))
	http.Handle("/metrics", promhttp.Handler())
}

func (s *Service) Init(dbc DBConfig) error {
	// Setup Logger
	log.SetHandler(apexJson.New(os.Stderr))
	s.Logger = log.WithFields(log.Fields{
		"service": s.Name,
		"env":     s.Environment,
	})

	// Setup Database
	var err error
	s.DB, err = SetupDB(dbc, s.Logger)
	if err != nil {
		s.Logger.Fatal("Error Setting up DB Connection")
	}

	// Setup Metrics
	InitializeMetrics()

	// Setup Router
	s.Router = Router{handler: http.DefaultServeMux}
	s.initializeRoutes()

	return nil
}

func (s *Service) Run(port string) {
	s.Logger.Info("Starting User Service on port " + port)
	defer s.DB.Close()
	s.Logger.Fatal(http.ListenAndServe(port, s.Router.Handler(s.Logger)).Error())
}

func NewService(env string, name string) Service {
	return Service{
		Name:        name,
		Environment: env,
	}
}

type Router struct {
	handler http.Handler
}

func (rtr *Router) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	rtr.handler.ServeHTTP(w, r)
}

func (rtr *Router) Handler(Logger log.Interface) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		rtr.ServeHTTP(w, r)
		Logger.WithFields(log.Fields{
			"remoteAddr": r.RemoteAddr,
			"method":     r.Method,
			"url":        r.URL.Path,
			"duration":   time.Now().Sub(start),
		}).Info("request")
	})
}
