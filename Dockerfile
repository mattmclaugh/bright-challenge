FROM golang:1.11

WORKDIR /go/src/app
COPY . .

RUN go install -v ./...

CMD ["app"]
