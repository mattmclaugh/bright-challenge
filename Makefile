test: starttestdc runtest stopdc

start: startbuilddc

stop: stopdc

coverage: starttestdc runtestcov stopdc
	go tool cover -html=cp.out

startbuilddc:
	docker-compose up -d --build

starttestdc:
	docker-compose up -d --scale user=0 --scale prometheus=0

stopdc:
	docker-compose down

runtest:
	go test -cover

runtestcov:
	go test -coverprofile cp.out
