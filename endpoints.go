package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"net/http"
	"regexp"
	"strconv"
)

func (s *Service) UsersHandler() http.Handler {
	return http.HandlerFunc(s.UsersEndpoint)
}

func (s *Service) UserHandler() http.Handler {
	return http.HandlerFunc(s.UserEndpoint)
}

func (s *Service) UsersEndpoint(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		users, err := GetUsers(s.DB)
		if err != nil {
			s.Logger.Error(err.Error())
			respondWithError(w, http.StatusInternalServerError, "Internal Server Error")
			return
		}

		respondWithJSON(w, http.StatusOK, users)
		return
	default:
		respondWithError(w, http.StatusInternalServerError, "Method Not Supported")
	}
}

func (s *Service) UserEndpoint(w http.ResponseWriter, r *http.Request) {
	switch r.Method {

	case http.MethodGet:
		id, err := parseUserId(r.URL.Path)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, err.Error())
			return
		}

		user, err := getUser(s.DB, id)
		if err != nil {
			s.handleGetUserError(err, user, w)
			return
		}

		respondWithJSON(w, http.StatusOK, user)
		return

	case http.MethodPut:
		id, err := parseUserId(r.URL.Path)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, err.Error())
			return
		}

		user, err := getUser(s.DB, id)
		if err != nil {
			s.handleGetUserError(err, user, w)
			return
		}

		decoder := json.NewDecoder(r.Body)
		if err := decoder.Decode(&user); err != nil {
			respondWithError(w, http.StatusBadRequest, "Invalid request payload")
			return
		}
		defer r.Body.Close()

		if err = user.Update(s.DB); err != nil {
			s.Logger.Error(err.Error())
			respondWithError(w, http.StatusInternalServerError, "Internal Server Error")
			return
		}

		respondWithJSON(w, http.StatusOK, user)
		return

	case http.MethodDelete:
		id, err := parseUserId(r.URL.Path)
		if err != nil {
			respondWithError(w, http.StatusBadRequest, err.Error())
			return
		}

		user, err := getUser(s.DB, id)
		if err != nil {
			s.handleGetUserError(err, user, w)
			return
		}

		if err = user.Delete(s.DB); err != nil {
			s.Logger.Error(err.Error())
			respondWithError(w, http.StatusInternalServerError, "Internal Server Error")
			return
		}

		respondWithJSON(w, http.StatusOK, User{})
		return

	case http.MethodPost:
		var u User
		decoder := json.NewDecoder(r.Body)
		if err := decoder.Decode(&u); err != nil {
			respondWithError(w, http.StatusBadRequest, "Invalid request payload")
			return
		}
		defer r.Body.Close()

		if err := u.Create(s.DB); err != nil {
			s.Logger.Error(err.Error())
			respondWithError(w, http.StatusInternalServerError, "Internal Server Error")
			return
		}

		respondWithJSON(w, http.StatusCreated, u)
		return
	}

	respondWithError(w, http.StatusNotFound, "Not Found")
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func parseUrl(rgx *regexp.Regexp, url string) map[string]string {
	match := rgx.FindStringSubmatch(url)
	result := make(map[string]string)
	for i, name := range rgx.SubexpNames() {
		if i != 0 && i < len(match) && name != "" {
			result[name] = match[i]
		}
	}
	return result
}

var userUrl = regexp.MustCompile(`/user/(?P<id>\d+)`)

func parseUserId(path string) (int, error) {
	url := parseUrl(userUrl, path)
	if _, ok := url["id"]; ok == true {
		id, err := strconv.Atoi(url["id"])
		if err == nil {
			return id, nil
		}
	}

	return 0, errors.New("Invalid User ID")
}

func (s *Service) handleGetUserError(err error, user User, w http.ResponseWriter) {
	switch err {
	case sql.ErrNoRows:
		respondWithJSON(w, http.StatusNotFound, user)
	default:
		s.Logger.Error(err.Error())
		respondWithError(w, http.StatusInternalServerError, "Internal Server Error")
	}
}
