package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"strconv"
	"testing"
)

var s Service

func TestMain(m *testing.M) {
	s = NewService("user-test", "test")

	dbc := DBConfig{
		Host:     "localhost",
		Port:     5432,
		User:     "postgres",
		Password: "password",
		Database: "user",
	}
	s.Init(dbc)

	exitCode := m.Run()

	clearDB()
	os.Exit(exitCode)
}

func TestBadUsersRoutes(t *testing.T) {
	clearDB()
	req, _ := http.NewRequest("GET", "/userssomething", nil)
	actual := executeRequest(req)
	checkResponseCode(t, http.StatusNotFound, actual.Code)
}

func TestNoUnknownUsersRoutes(t *testing.T) {
	clearDB()
	req, _ := http.NewRequest("GET", "/users/something", nil)
	actual := executeRequest(req)
	checkResponseCode(t, http.StatusNotFound, actual.Code)
}

func TestNoUsers(t *testing.T) {
	clearDB()
	req, _ := http.NewRequest("GET", "/users", nil)
	actual := executeRequest(req)
	expected := "[]"

	checkResponseCode(t, http.StatusOK, actual.Code)
	if body := actual.Body.String(); body != expected {
		t.Errorf("Expected an empty array. Got %s", body)
	}
}

func TestInvalidUserID(t *testing.T) {
	clearDB()
	req, _ := http.NewRequest("GET", "/user/fourtytwo", nil)
	actual := executeRequest(req)
	expected := `{"error":"Invalid User ID"}`

	checkResponseCode(t, http.StatusBadRequest, actual.Code)
	if body := actual.Body.String(); body != expected {
		t.Errorf("Expected an empty object. Got %s", body)
	}
}

func TestNoSuchUser(t *testing.T) {
	clearDB()
	req, _ := http.NewRequest("GET", "/user/42", nil)
	actual := executeRequest(req)
	expected := "{}"

	checkResponseCode(t, http.StatusNotFound, actual.Code)
	if body := actual.Body.String(); body != expected {
		t.Errorf("Expected an empty object. Got %s", body)
	}
}

func TestGetRealUser(t *testing.T) {
	clearDB()
	addUsers(1)

	req, _ := http.NewRequest("GET", "/user/1", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
}

func TestUpdateUser(t *testing.T) {
	clearDB()
	addUsers(1)

	req, _ := http.NewRequest("GET", "/user/1", nil)
	resp1 := executeRequest(req)

	var originalUser map[string]interface{}
	json.Unmarshal(resp1.Body.Bytes(), &originalUser)

	payload := []byte(`{"firstName":"Updated Name"}`)

	req, _ = http.NewRequest("PUT", "/user/1", bytes.NewBuffer(payload))
	resp2 := executeRequest(req)
	checkResponseCode(t, http.StatusOK, resp2.Code)

	var m map[string]interface{}
	json.Unmarshal(resp2.Body.Bytes(), &m)

	if m["id"] != originalUser["id"] {
		t.Errorf("Expected the id to remain the same (%v). Got %v", originalUser["id"], m["id"])
	}

	if m["firstName"] == originalUser["firstName"] {
		t.Errorf("Expected the name to change from '%v' to '%v'. Got '%v'", originalUser["firstName"], m["firstName"], m["firstName"])
	}
}

func TestDeleteUser(t *testing.T) {
	clearDB()
	addUsers(1)

	req, _ := http.NewRequest("DELETE", "/user/1", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)

	q := "SELECT count(id) FROM user_account"
	var count int
	s.DB.QueryRow(q).Scan(&count)
	if count != 0 {
		t.Errorf("Exect count of users to be '0'. Got '%v'", count)
	}
}

func TestDeleteMissingUser(t *testing.T) {
	clearDB()
	addUsers(1)

	req, _ := http.NewRequest("DELETE", "/user/42", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusNotFound, response.Code)
}

func TestCreateUser(t *testing.T) {
	clearDB()

	payload := []byte(`{"firstName":"test","lastName": "user","email": "testuser@testy.test","zipCode": "97209"}`)

	req, _ := http.NewRequest("POST", "/user", bytes.NewBuffer(payload))
	actual := executeRequest(req)

	checkResponseCode(t, http.StatusCreated, actual.Code)
	var m map[string]interface{}
	json.Unmarshal(actual.Body.Bytes(), &m)

	if m["id"] != 1.0 {
		t.Errorf("Expected user id to be '1'. Got '%v'", m["id"])
	}

	if m["lastName"] != "user" {
		t.Errorf("Expected user last name to be 'user'. Got '%v'", m["lastName"])
	}

	if m["firstName"] != "test" {
		t.Errorf("Expected user first name to be 'test'. Got '%v'", m["firstName"])
	}

	if m["email"] != "testuser@testy.test" {
		t.Errorf("Expected user email to be 'testuser@testy.test'. Got '%v'", m["email"])
	}

	if m["zipCode"] != "97209" {
		t.Errorf("Expected user zip to be '97209'. Got '%v'", m["zipCode"])
	}
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	s.Router.ServeHTTP(rr, req)

	return rr
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func addUsers(count int) {
	if count < 1 {
		count = 1
	}
	for i := 0; i < count; i++ {
		statement := fmt.Sprintf(
			"INSERT INTO user_account(first_name, last_name, email, zip_code) VALUES('%s','%s','%s','%s')",
			("First" + strconv.Itoa(i+1)),
			("Last" + strconv.Itoa(i+1)),
			"email", "11111")

		s.DB.Exec(statement)
	}
}

func clearDB() {
	s.DB.Exec("TRUNCATE user_account RESTART IDENTITY")
}
