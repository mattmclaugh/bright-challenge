# User Service
This is a simple user service that supports GET, POST, PUT and DELETE on a user
object and a GET of all users.

## Overview
This service is initiated in `main.go` it utilizes the default golang http mux
but wraps the default Route handler for logging purposes. Handlers adapt
HandlerFunc's so they can be wrapped with middleware (like the MetricsHandler)
while Endpoints manage which actions to take per method.

## Running
You must have Docker installed and be running in an invironment that supports
Make (osx, \*nix)

To run the service and it's supporting services (this make take a few minutes
the first time it's ran)
```
make start
```

To stop all services
```
make stop
```

Creating a new user:
```
curl -d '{"firstName":"test", "lastName":"test", "email":"email.com", "zipCode": "11111"}' -H "Content-Type: application/json" -X POST http://localhost:10000/user
```

Getting all users:
```
curl localhost:10000/users
```

Getting a single user
```
curl localhost:10000/user/1
```

Updating a user
```
curl -d '{"firstName":"test", "lastName":"2test", "email":"2email.com", "zipCode": "121111"}' -H "Content-Type: application/json" -X PUT http://localhost:10000/user/1
```

Deleting a user
```
curl -X DELETE localhost:10000/user/1
```

## Testing
These tests are closer in kind to integration tests than true unit tests but
given the limited amount of business logic I felt they exercised the code
sufficently. What is not being tested here is the Prometheus metrics endpoint the 
structured logging and mid request database failures.

Running tests requires you have go 1.9+ installed.

To run route tests
```
make test
```

To generate and view coverage report
```
make coverage
```

## What could be improved upon

*Validation*: This service assums the user is submitting the correct object
format. While it won't create attributes it's not expecting, it doesn't require
all properties be present. It also doesn't enforce type constraints for things
like `email` or `zipCode`.

*Structure*: The endpoints file is too big and for the sake of readability it
could be broken down into smaller chunks which could also improve unit
testing.

*Configuration*: There is only one configurable parameter; the database host.
However it does demonstrate 12 Factor configuration. Ideally all of the database
parameters and service port would be configurable by the environment.
