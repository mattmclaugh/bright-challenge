package main

import (
	"database/sql"
	"fmt"
	"github.com/apex/log"
	_ "github.com/lib/pq"
	"time"
)

type DBConfig struct {
	Host     string
	Port     int
	User     string
	Password string
	Database string
}

func (dbc *DBConfig) ConnectionString() string {
	return fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		dbc.Host, dbc.Port, dbc.User, dbc.Password, dbc.Database)
}

func SetupDB(dbc DBConfig, Logger log.Interface) (*sql.DB, error) {
	db, err := sql.Open("postgres", dbc.ConnectionString())
	if err != nil {
		panic(err)
	}

	for {
		err := db.Ping()
		if err == nil {
			break
		}

		Logger.Info("Waiting for DB ...")
		time.Sleep(500 * time.Millisecond)
	}

	return db, nil
}
