package main

import (
	"database/sql"
)

type User struct {
	ID        int    `json:"id,omitempty"`
	FirstName string `json:"firstName,omitempty"`
	LastName  string `json:"lastName,omitempty"`
	Email     string `json:"email,omitempty"`
	ZipCode   string `json:"zipCode,omitempty"`
}

type Users []User

func getUser(db *sql.DB, id int) (User, error) {
	q := `
		SELECT id, first_name, last_name, email, zip_code
		FROM user_account
		WHERE id = $1`
	u := User{}
	err := db.QueryRow(q, id).Scan(
		&u.ID,
		&u.FirstName,
		&u.LastName,
		&u.Email,
		&u.ZipCode)

	return u, err
}

func (u *User) Update(db *sql.DB) error {
	q := `
		UPDATE user_account 
		SET 
			first_name = $1, 
			last_name = $2, 
			email = $3,
			zip_code = $4
		WHERE id = $5`

	_, err := db.Query(q,
		u.FirstName,
		u.LastName,
		u.Email,
		u.ZipCode,
		u.ID)
	return err
}

func (u *User) Delete(db *sql.DB) error {
	q := "DELETE FROM user_account WHERE id = $1"
	_, err := db.Query(q, u.ID)
	if err != nil {
		return err
	}

	return nil
}

func (u *User) Create(db *sql.DB) error {
	q := `
		INSERT INTO user_account (first_name, last_name, email, zip_code)
		VALUES ($1, $2, $3, $4) RETURNING id`

	stmt, err := db.Prepare(q)
	if err != nil {
		return err
	}
	defer stmt.Close()

	err = stmt.QueryRow(
		u.FirstName,
		u.LastName,
		u.Email,
		u.ZipCode,
	).Scan(&u.ID)
	if err != nil {
		return err
	}

	return nil
}

func GetUsers(db *sql.DB) (Users, error) {
	q := "SELECT id, first_name, last_name, email, zip_code FROM user_account"
	rows, err := db.Query(q)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users := Users{}
	for rows.Next() {
		var u User
		err := rows.Scan(
			&u.ID,
			&u.FirstName,
			&u.LastName,
			&u.Email,
			&u.ZipCode,
		)
		if err != nil {
			return nil, err
		}

		users = append(users, u)
	}
	return users, nil
}
