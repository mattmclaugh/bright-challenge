package main

import (
	"os"
)

func main() {
	dbHost := "localhost"
	if os.Getenv("DB_HOST") != "" {
		dbHost = os.Getenv("DB_HOST")
	}

	dbc := DBConfig{
		Host:     dbHost,
		Port:     5432,
		User:     "postgres",
		Password: "password",
		Database: "user",
	}

	service := NewService("prod", "user")
	service.Init(dbc)
	service.Run(":10000")
}
